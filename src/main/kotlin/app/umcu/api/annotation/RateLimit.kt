/*
 * UpcomingMCU API
 * Copyright (C) 2024 Sean O'Connor & UpcomingMCU Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package app.umcu.api.annotation

import app.umcu.api.UMCUProperties
import app.umcu.api.error.exception.RateLimitException
import io.github.bucket4j.BandwidthBuilder.BandwidthBuilderCapacityStage
import io.github.bucket4j.Bucket
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Before
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.time.Duration

@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class RateLimit

@Suppress("unused")
@Component
@Aspect
class RateLimitAspect(
	private val request: HttpServletRequest,
	private val response: HttpServletResponse,
	properties: UMCUProperties,
	private val logger: Logger = LoggerFactory.getLogger(RateLimitAspect::class.java),
) {

	private val rateLimitTokens = properties.app.rateLimit.tokensPerDuration.toLong()
	private val rateLimitDuration: Duration = Duration.ofMinutes(properties.app.rateLimit.durationMin.toLong())
	private val bucket = Bucket.builder().addLimit { limit: BandwidthBuilderCapacityStage ->
		limit.capacity(
			rateLimitTokens
		).refillIntervally(rateLimitTokens, rateLimitDuration)
	}.build()

	@Before("@annotation(RateLimit)")
	fun checkRateLimit() {
		val consumptionProbe = bucket.tryConsumeAndReturnRemaining(1)

		// Calculate how many minutes and seconds until the next token refill
		val secondsToWaitForReset = consumptionProbe.nanosToWaitForReset / 1_000_000_000
		val minutes = secondsToWaitForReset / 60
		val remainingSeconds = secondsToWaitForReset % 60
		val minutesSeconds = "${minutes.toString().padStart(2, '0')}:${remainingSeconds.toString().padStart(2, '0')}"

		// Inject headers into the response, indicating the status of the rate limit
		response.setHeader("X-RateLimit-Limit", rateLimitTokens.toString())
		response.setHeader("X-RateLimit-Remaining", consumptionProbe.remainingTokens.toString())
		response.setHeader("X-RateLimit-Reset", consumptionProbe.nanosToWaitForReset.toString())

		// If the consumption probe could not be consumed (i.e., no remaining tokens)
		if (!consumptionProbe.isConsumed) {
			throw RateLimitException(reason = "Too many requests. Try again in $minutesSeconds.")
		}
	}
}
