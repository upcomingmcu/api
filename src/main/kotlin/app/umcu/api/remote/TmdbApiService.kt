/*
 * UpcomingMCU API
 * Copyright (C) 2024 Sean O'Connor & UpcomingMCU Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package app.umcu.api.remote

import app.umcu.api.UMCUProperties
import app.umcu.api.feature.mediatype.MediaTypeService
import app.umcu.api.feature.production.Production
import app.umcu.api.remote.model.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder

@Component
class TmdbApiService(
    properties: UMCUProperties,
    private val restTemplate: RestTemplate = RestTemplate(),
    private val mediaTypeService: MediaTypeService,
    private val logger: Logger = LoggerFactory.getLogger(TmdbApiService::class.java),
) {

	@Suppress("PrivatePropertyName")
	private val BASE_URL = "https://api.themoviedb.org/3/"

	@Suppress("PrivatePropertyName")
	private val API_KEY = properties.tmdb.tokenField

	/**
	 * Perform a GET request to the TMDB API.
	 *
	 * @param T The return type of the request.
	 * @param pathSegments Path segments to be appended to the end of the [BASE_URL].
	 * @param params Query parameters to be appended to the end of the entire url.
	 * @return [T]
	 * @throws NullPointerException If an error occurs while performing the request.
	 */
	private inline fun <reified T : Any> req(
		pathSegments: Array<String> = emptyArray(),
		params: List<Pair<String, String>> = emptyList(),
	): T {
		val headers = HttpHeaders()
		headers.set("Authorization", "Bearer $API_KEY")
		val httpEntity = HttpEntity("body", headers)

		val uriBuilder = UriComponentsBuilder.fromUriString(BASE_URL).pathSegment(*pathSegments)
		params.forEach { uriBuilder.queryParam(it.first, it.second) }
		val uri = uriBuilder.build().toUri()

		return try {
			val response = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, T::class.java)
			logger.debug("HTTP {} \'{}\'", response.statusCode, uri.toString())
			response.body!!
		} catch (e: Exception) {
			throw NullPointerException("Could not obtain request from '$uri' -> \"${e.message ?: "An unknown error occurred"}\"")
		}
	}

	private fun getListPage(listId: Int, page: Int) = req<ListDetails>(
		pathSegments = arrayOf("list", listId.toString()), params = listOf(Pair("page", page.toString()))
	)

	private fun getMovie(movieId: Int) = req<MovieDetails>(
		pathSegments = arrayOf("movie", movieId.toString()),
		params = listOf(Pair("append_to_response", "external_ids,release_dates"))
	)

	private fun getSeries(seriesId: Int) = req<SeriesDetails>(
		pathSegments = arrayOf("tv", seriesId.toString()), params = listOf(Pair("append_to_response", "external_ids"))
	)

	fun getList(listId: Int): ArrayList<ListItem> {
		val items = ArrayList<ListItem>()
		var page = 1
		do {
			val list = getListPage(listId, page)
			items.addAll(list.listItems)
			page += 1
		} while (list.page != list.totalPages)
		return items
	}

	private fun getMovieReleaseDate(movieDetails: MovieDetails): String? {
		val dates = movieDetails.releaseDates.results.find { it.iso31661 == "US" }?.releaseDates ?: return null
		return dates.find { it.type == ReleaseType.THEATRICAL }?.releaseDate
			?: dates.find { it.type == ReleaseType.DIGITAL }?.releaseDate
	}

	private fun collectMovies(listItems: ArrayList<ListItem>): ArrayList<MovieDetails> {
		val movies = ArrayList<MovieDetails>()
		val movieIds = listItems.filter { it.mediaType == "movie" }.map { it.tmdbId }
		movieIds.forEach { id -> movies.add(getMovie(id)) }
		return movies
	}

	private fun collectSeries(listItems: ArrayList<ListItem>): ArrayList<SeriesDetails> {
		val series = ArrayList<SeriesDetails>()
		val seriesIds = listItems.filter { it.mediaType == "tv" }.map { it.tmdbId }
		seriesIds.forEach { id -> series.add(getSeries(id)) }
		return series
	}

	fun collectAll(listItems: ArrayList<ListItem>): ArrayList<Production> {
		val productions = ArrayList<Production>()

		val collectedMovies = collectMovies(listItems)
		val collectedSeries = collectSeries(listItems)

		// Add each movie to the database
		collectedMovies.map { movie ->
			Production(
				tmdbId = movie.tmdbId,
				imdbId = movie.externalIds.imdbId,
				title = movie.title,
				releaseDate = getMovieReleaseDate(movie),
				overview = movie.overview,
				posterPath = movie.posterPath,
				mediaType = mediaTypeService.findByName("movie")
			)
		}.forEach { productions.add(it) }

		// Add each series to the database
		collectedSeries.forEach { series ->
			if (series.seasons.size > 1) {
				// Add each season of the series
				series.seasons.map { season ->
					val title = "${series.name} Season ${season.seasonNumber}"
					val overview = if (season.overview.isNullOrBlank()) series.overview else season.overview
					val posterPath = if (season.posterPath.isNullOrBlank()) series.posterPath else season.posterPath

					Production(
						tmdbId = series.tmdbId,
						imdbId = series.externalIds.imdbId,
						title = title,
						releaseDate = season.airDate,
						overview = overview,
						posterPath = posterPath,
						mediaType = mediaTypeService.findByName("tv")
					)
				}.forEach { productions.add(it) }
			} else {
				// Add an entire series
				val production = Production(
					tmdbId = series.tmdbId,
					imdbId = series.externalIds.imdbId,
					title = series.name,
					releaseDate = series.firstAirDate,
					overview = series.overview,
					posterPath = series.posterPath,
					mediaType = mediaTypeService.findByName("tv")
				)
				productions.add(production)
			}
		}

		return productions
	}
}
