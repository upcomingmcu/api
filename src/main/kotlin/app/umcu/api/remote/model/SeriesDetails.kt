/*
 * UpcomingMCU API
 * Copyright (C) 2024 Sean O'Connor & UpcomingMCU Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package app.umcu.api.remote.model

import com.fasterxml.jackson.annotation.JsonProperty

data class SeriesDetails(
	@JsonProperty("first_air_date") val firstAirDate: String?,
	@JsonProperty("id") val tmdbId: Int,
	val name: String,
	val overview: String?,
	@JsonProperty("poster_path") val posterPath: String?,
	val seasons: List<SeasonDetails>,
	@JsonProperty("external_ids") val externalIds: ExternalIds,
) {

	data class SeasonDetails(
		@JsonProperty("air_date") val airDate: String?,
		@JsonProperty("episode_count") val episodeCount: Int?,
		@JsonProperty("id") val tmdbId: Int,
		val name: String?,
		val overview: String?,
		@JsonProperty("poster_path") val posterPath: String?,
		@JsonProperty("season_number") val seasonNumber: Int,
	)
}
