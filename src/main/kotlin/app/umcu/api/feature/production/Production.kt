/*
 * UpcomingMCU API
 * Copyright (C) 2024 Sean O'Connor & UpcomingMCU Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package app.umcu.api.feature.production

import app.umcu.api.feature.mediatype.MediaType
import app.umcu.api.feature.production.dto.NextProductionDto
import app.umcu.api.feature.production.dto.ProductionDto
import app.umcu.api.util.DateUtils
import app.umcu.api.util.ext.toSlug
import jakarta.persistence.*
import java.time.Instant

/**
 * @property id A unique identifier for the production.
 * @property slug A unique, human-friendly, identifier for the production.
 * @property tmdbId The production's associated ID on TMDB.
 * @property imdbId The production's associated ID on IMDB. Can be null.
 * @property title The official title of the production.
 * @property releaseDate The US theatrical release date of the production. Can be null.
 * @property overview A brief overview of the production. Can be null.
 * @property posterPath The path of the production's poster. Can be null.
 * @property mediaType The type of production, i.e., "movie" or "tv".
 */
@Entity
@Table(name = "productions")
data class Production(
	@Id @GeneratedValue val id: Long? = null,
	@Column(unique = true, nullable = false) var slug: String? = null,
	@Column(nullable = false) val tmdbId: Int,
	@Column(length = 10) val imdbId: String?,
	@Column(nullable = false, unique = true) val title: String,
	@Column val releaseDate: Instant?,
	@Column(columnDefinition = "TEXT") val overview: String?,
	@Column val posterPath: String?,
	@ManyToOne val mediaType: MediaType?,
) {

	/**
	 * @param tmdbId The production's associated ID on TMDB.
	 * @param imdbId The production's associated ID on IMDB. Can be null.
	 * @param title The official title of the production.
	 * @param releaseDate The US theatrical release date of the production. Can be null.
	 * @param overview A brief overview of the production. Can be null.
	 * @param posterPath The image file of the production's poster. Will be combined with the full URL. Can be null.
	 * @param mediaType The type of production, i.e., "movie" or "tv".
	 */
	constructor(
		tmdbId: Int,
		imdbId: String?,
		title: String,
		releaseDate: String?,
		overview: String?,
		posterPath: String?,
		mediaType: MediaType?,
	) : this(
		id = null, // Will be automatically set when the entity is saved to the database.
		slug = title.toSlug(),
		tmdbId = tmdbId,
		imdbId = imdbId,
		title = title,
		releaseDate = DateUtils.atStartOfDay(releaseDate),
		overview = overview,
		posterPath = posterPath,
		mediaType = mediaType
	)
}

/**
 * Convert a [Production] entity into a [ProductionDto] object.
 */
fun Production.dto() = ProductionDto(this)

/**
 * Convert a [Production] entity into a [NextProductionDto] object with an associated [previous] and [next].
 *
 * @param previous The url of the production that released before this [Production]. Can be null.
 * @param next The url of the production that releases after this [Production]. Can be null.
 */
fun Production.nextDto(previous: String? = null, next: String? = null) = NextProductionDto(
	production = this.dto(), previous = previous, next = next
)
