/*
 * UpcomingMCU API
 * Copyright (C) 2024 Sean O'Connor & UpcomingMCU Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

@file:Suppress("unused")

package app.umcu.api.feature.production

import app.umcu.api.annotation.RateLimit
import app.umcu.api.error.exception.ProductionNotFoundException
import app.umcu.api.feature.production.dto.ListDto
import app.umcu.api.feature.production.dto.NextProductionDto
import app.umcu.api.feature.production.dto.ProductionDto
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/productions")
class ProductionController(private val service: ProductionService) {

	@GetMapping
	@RateLimit
	fun getAllProductions(@RequestParam(required = false) filter: String?): ListDto<ProductionDto> {
		return service.getAllProductions(filter)
	}

	@GetMapping("/{slug}")
	@RateLimit
	fun getProductionBySlug(@PathVariable slug: String): ProductionDto {
		return service.getProductionBySlug(slug) ?: throw ProductionNotFoundException()
	}

	@GetMapping("/next")
	@RateLimit
	fun getNextProduction(@RequestParam(required = false) date: String?): NextProductionDto {
		return service.getNextProduction(date) ?: throw ProductionNotFoundException()
	}
}
