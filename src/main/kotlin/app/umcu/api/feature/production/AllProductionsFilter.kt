/*
 * UpcomingMCU API
 * Copyright (C) 2024 Sean O'Connor & UpcomingMCU Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package app.umcu.api.feature.production

enum class AllProductionsFilter {
	ALL,
	RELEASED,
	UPCOMING,
	ANNOUNCED;

	companion object {
		fun parse(filterString: String?): AllProductionsFilter {
			return if (filterString.isNullOrEmpty()) ALL
			else {
				return try {
					valueOf(filterString.uppercase())
				} catch (e: Exception) {
					ALL
				}
			}
		}
	}
}
