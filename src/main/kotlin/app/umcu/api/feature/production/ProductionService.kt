/*
 * UpcomingMCU API
 * Copyright (C) 2024 Sean O'Connor & UpcomingMCU Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package app.umcu.api.feature.production

import app.umcu.api.UMCUProperties
import app.umcu.api.feature.production.dto.ListDto
import app.umcu.api.feature.production.dto.NextProductionDto
import app.umcu.api.feature.production.dto.ProductionDto
import app.umcu.api.util.DateUtils
import org.springframework.stereotype.Service
import java.time.Instant
import java.time.temporal.ChronoUnit
import kotlin.jvm.optionals.getOrNull

@Service
class ProductionService(
	private val productionRepository: ProductionRepository,
	private val properties: UMCUProperties,
) {

	private fun sorted() = productionRepository.findAll().sortedWith(compareBy { it.releaseDate ?: Instant.MAX })

	fun saveAll(productions: Iterable<Production>): List<Production> {
		return productionRepository.saveAll(productions).toList()
	}

	fun getAllProductions(filter: String?): ListDto<ProductionDto> {
		val productions = sorted().map { it.dto() }
		// Filter `productions` based upon the `filter` string parsed by `AllProductionsFilter`
		val filteredProductions = when (AllProductionsFilter.parse(filter)) {
			AllProductionsFilter.ALL -> productions
			AllProductionsFilter.RELEASED -> productions.filter { it.releaseDate?.isBefore(Instant.now()) ?: false }
			AllProductionsFilter.UPCOMING -> productions.filter { it.releaseDate?.isAfter(Instant.now()) ?: false }
			AllProductionsFilter.ANNOUNCED -> productions.filter { it.releaseDate == null }
		}
		return ListDto(filteredProductions)
	}

	fun getProductionBySlug(slug: String): ProductionDto? {
		return productionRepository.findBySlug(slug).map { it.dto() }.getOrNull()
	}

	fun getNextProduction(dateString: String?): NextProductionDto? {
		val date = DateUtils.atStartOfDay(dateString) ?: Instant.now()
		val productions = sorted()

		val productionIndex = productions.indexOfFirst { it.releaseDate?.isAfter(date) ?: false }
		if (productionIndex == -1) return null
		val production = productions[productionIndex]

		val baseUrl = properties.app.baseUrl.removeSuffix("/")

		val previousProduction = productions.getOrNull(productionIndex - 1)?.takeIf { it.releaseDate != null }
		val previousProductionUrl =
			if (previousProduction?.releaseDate == null) null else "${baseUrl}/api/productions/next?date=${
				previousProduction.releaseDate.minus(
					1, ChronoUnit.DAYS
				)
			}"

		val nextProduction = productions.getOrNull(productionIndex + 1)?.takeIf { it.releaseDate != null }
		val nextProductionUrl =
			if (nextProduction?.releaseDate == null) null else "${baseUrl}/api/productions/next?date=${production.releaseDate}"

		return production.nextDto(
			previous = previousProductionUrl, next = nextProductionUrl
		)
	}
}
