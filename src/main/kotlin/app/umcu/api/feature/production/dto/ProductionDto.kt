/*
 * UpcomingMCU API
 * Copyright (C) 2024 Sean O'Connor & UpcomingMCU Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package app.umcu.api.feature.production.dto

import app.umcu.api.feature.production.Production
import com.fasterxml.jackson.annotation.JsonProperty
import java.time.Instant

/**
 * @property slug A unique, human-friendly, identifier for the production.
 * @property tmdbId The production's associated ID on TMDB.
 * @property imdbId The production's associated ID on IMDB. Can be null.
 * @property title The official title of the production.
 * @property releaseDate The US theatrical release date of the production. Can be null.
 * @property overview A brief overview of the production. Can be null.
 * @property posterPath The path of the production's poster. Can be null.
 * @property mediaType The type of production, i.e., "movie" or "tv".
 */
data class ProductionDto(
	val slug: String,
	@JsonProperty("tmdb_id") val tmdbId: Int,
	@JsonProperty("imdb_id") val imdbId: String?,
	val title: String,
	@JsonProperty("release_date") val releaseDate: Instant?,
	val overview: String?,
	@JsonProperty("poster_path") val posterPath: String?,
	@JsonProperty("media_type") val mediaType: String?,
) {

	/**
	 * @param production Instance of a [Production] entity.
	 */
	constructor(production: Production) : this(
		slug = production.slug!!, // Slug cannot be null.
		tmdbId = production.tmdbId,
		imdbId = production.imdbId,
		title = production.title,
		releaseDate = production.releaseDate,
		overview = production.overview,
		posterPath = production.posterPath,
		mediaType = production.mediaType?.name
	)
}
