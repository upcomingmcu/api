/*
 * UpcomingMCU API
 * Copyright (C) 2024 Sean O'Connor & UpcomingMCU Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package app.umcu.api

import io.swagger.v3.oas.annotations.ExternalDocumentation
import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.info.Contact
import io.swagger.v3.oas.annotations.info.Info
import io.swagger.v3.oas.annotations.info.License
import io.swagger.v3.oas.annotations.servers.Server
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication
@EnableConfigurationProperties(UMCUProperties::class)
@OpenAPIDefinition(
	info = Info(
		title = OpenApiDef.INFO_TITLE,
		version = OpenApiDef.INFO_VERSION,
		description = OpenApiDef.INFO_DESCRIPTION,
		license = License(name = OpenApiDef.LICENSE_NAME, url = OpenApiDef.LICENSE_URL),
		contact = Contact(url = OpenApiDef.CONTACT_URL, email = OpenApiDef.CONTACT_EMAIL)
	),
	externalDocs = ExternalDocumentation(description = OpenApiDef.EXL_DOC_DESCRIPTION, url = OpenApiDef.EXT_DOC_URL),
	servers = [Server(description = OpenApiDef.SERVER_PROD_DESCRIPTION, url = OpenApiDef.SERVER_PROD_URL), Server(
		description = OpenApiDef.SERVER_DEV_DESCRIPTION, url = OpenApiDef.SERVER_DEV_URL
	)]
)
class Application

fun main(args: Array<String>) {
	runApplication<Application>(*args)
}
