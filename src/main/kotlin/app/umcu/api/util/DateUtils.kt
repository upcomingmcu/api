/*
 * UpcomingMCU API
 * Copyright (C) 2024 Sean O'Connor & UpcomingMCU Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package app.umcu.api.util

import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException

object DateUtils {
	private val ZONE = ZoneId.of("America/New_York")

	private fun trimDateString(dateString: String?): String? {
		if (dateString == null) return null
		if (dateString.length < 10) return null
		val trimmed = dateString.take(10)
		return trimmed
	}

	fun atStartOfDay(dateString: String?): Instant? {
		if (dateString == null) return null
		val trimmedDateString = trimDateString(dateString) ?: return null
		return try {
			val localDate = dateString.let { LocalDate.parse(trimmedDateString, DateTimeFormatter.ISO_LOCAL_DATE) }
			val zonedDateTime = localDate.atStartOfDay(ZONE)
			zonedDateTime.toInstant()
		} catch (e: DateTimeParseException) {
			null
		}
	}
}
