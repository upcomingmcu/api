/*
 * UpcomingMCU API
 * Copyright (C) 2024 Sean O'Connor & UpcomingMCU Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package app.umcu.api.init

import app.umcu.api.UMCUProperties
import app.umcu.api.feature.mediatype.MediaTypeService
import app.umcu.api.feature.production.ProductionService
import app.umcu.api.remote.TmdbApiService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component

@Suppress("unused", "LoggingSimilarMessage")
@Component
class InitProductions(
	private val properties: UMCUProperties,
	private val tmdbApiService: TmdbApiService,
	private val productionService: ProductionService,
	private val mediaTypeService: MediaTypeService,
	private val logger: Logger = LoggerFactory.getLogger(InitProductions::class.java),
) : ApplicationRunner {

	override fun run(args: ApplicationArguments?) {
		populateMediaTypes()
		populateProductions()
	}

	private fun populateMediaTypes() {
		logger.info("Defining media types.")
		val saved = mediaTypeService.saveAll(listOf("movie", "tv"))
		logger.info("Saved ${saved.count()} media types to the database.")
	}

	private fun populateProductions() {
		logger.info("Obtaining data from TMDB.")
		val listItems = tmdbApiService.getList(properties.tmdb.listId)
		val productions = tmdbApiService.collectAll(listItems)

		logger.info("Saving ${productions.size} productions to the database.")
		val saved = productionService.saveAll(productions)
		logger.info("Saved ${saved.count()}.")
	}
}
