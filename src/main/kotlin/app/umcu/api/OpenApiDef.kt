/*
 * UpcomingMCU API
 * Copyright (C) 2024 Sean O'Connor & UpcomingMCU Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package app.umcu.api

/**
 * If you plan on hosting your own instance of this API, *please* change these values!
 */
object OpenApiDef {

	const val INFO_TITLE = "UpcomingMCU API"
	const val INFO_VERSION = "2.1.1"
	const val INFO_DESCRIPTION = "The ultimate source for staying up-to-date with the Marvel Cinematic Universe (MCU)."

	const val LICENSE_NAME = "GNU Affero General Public License v3.0"
	const val LICENSE_URL = "https://gitlab.com/upcomingmcu/api/-/blob/main/LICENSE"

	const val CONTACT_URL = "https://gitlab.com/upcomingmcu/api"
	const val CONTACT_EMAIL = "api@umcu.app"

	const val EXL_DOC_DESCRIPTION = "Source code"
	const val EXT_DOC_URL = "https://gitlab.com/upcomingmcu/api"

	const val SERVER_PROD_DESCRIPTION = "Production"
	const val SERVER_PROD_URL = "https://umcu.app/"
	const val SERVER_DEV_DESCRIPTION = "Development"
	const val SERVER_DEV_URL = "http://localhost:8080/"
}
