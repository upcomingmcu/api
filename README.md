# UpcomingMCU API

![GitLab Latest Release](https://img.shields.io/gitlab/v/release/upcomingmcu%2Fapi?label=latest%20release&style=flat&color=green) ![GitLab Stars](https://img.shields.io/gitlab/stars/upcomingmcu%2Fapi?style=flat&color=yellow) ![GitLab License](https://img.shields.io/gitlab/license/upcomingmcu%2Fapi?style=flat)

The ultimate source for staying up-to-date with the Marvel Cinematic Universe (MCU).

This API is built with Spring Boot and written in Kotlin. For a full list of dependencies used, please
see [gradle/libs.versions.toml](gradle/libs.versions.toml).

### ➡️ [Access the API here.](https://umcu.app/)

## Documentation

[**OpenAPI v3.0.1 specs**](https://api.umcu.app/openapi)

This is a **consumption-only** API (meaning you may only make GET requests) and no authentication is required. In
addition, there is a **rate limit of 3600 requests per hour**. The rate limit can (and probably will be) modified in the
future if needed.

### Endpoints

The following endpoints are available:

#### ⭐  `GET /api/productions`

Get all productions within the MCU. Returns `ListDto`.

Can be filtered by using the optional `filter` parameter, which accepts `all`, `released`, `upcoming`, or `announced`.

#### ⭐ `GET /api/productions/:slug`

Get a single production by its slug. Returns `ProductionDto`.

If the slug is not valid, a 404 status is returned.

#### ⭐ `GET /api/productions/next`

Get the next production to-be-released. Returns `NextProductionDto`.

An optional `date` parameter can be provided, which accepts an ISO 8061 formatted date. The production returned will be
the next production released after this date.

If there is no production upcoming, a 404 status is returned.

### Models

Note: The use of a quotation mark at the end of a data type (`string?`) indicates the property is nullable.

#### ListDto

```json
{
  "count": "string",
  "results": "list[ProductionDto]"
}
```

#### ProductionDto

```json
{
  "slug": "string",
  "tmdb_id": "int",
  "imdb_id": "string?",
  "title": "string",
  "release_date": "string?",
  "overview": "string?",
  "poster_path": "string?",
  "media_type": "string"
}
```

#### NextProductionDto

```json
{
  "production": "ProductionDto",
  "previous": "string?",
  "next": "string?"
}
```

## License

UpcomingMCU API is released under the GNU Affero General Public License ([full text](LICENSE)).

Copyright (C) 2024 Sean O'Connor & UpcomingMCU Contributors.
