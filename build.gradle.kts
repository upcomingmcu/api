@file:Suppress("VulnerableLibrariesLocal")

import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	alias(libs.plugins.spring.boot)
	alias(libs.plugins.spring.dependency.management)
	alias(libs.plugins.kotlin.jvm)
	alias(libs.plugins.kotlin.spring)
	alias(libs.plugins.kotlin.jpa)
}

group = "app.umcu"
version = "0.0.1-SNAPSHOT"

java {
	sourceCompatibility = JavaVersion.VERSION_21
}

dependencies {
	implementation(libs.spring.boot.data.jpa)
	implementation(libs.spring.boot.web)
	implementation(libs.spring.boot.aop)
	implementation(libs.jackson)
	implementation(libs.kotlin.reflect)
	implementation(libs.springdoc)
	implementation(libs.bucket4j)

	runtimeOnly(libs.h2)
	runtimeOnly(libs.postgresql)
	testImplementation(libs.spring.boot.test)
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs += "-Xjsr305=strict"
		jvmTarget = "21"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}
