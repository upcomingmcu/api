FROM gradle:8.4.0-jdk21 AS build

RUN useradd -rm -d /home/builder -s /bin/bash -g root -G sudo -u 1001 builder
USER builder
WORKDIR /home/builder/

COPY gradle gradle/
COPY src src/
COPY build.gradle.kts .
COPY gradlew .
COPY settings.gradle.kts .

RUN ./gradlew bootJar

FROM eclipse-temurin:21-jre

RUN useradd -rm -d /home/runner -s /bin/bash -g root -G sudo -u 1001 runner
USER runner
WORKDIR /home/runner/

COPY --from=build /home/builder/build build/
CMD ["java", "-jar", "./build/libs/upcomingmcu-api-0.0.1-SNAPSHOT.jar"]  
