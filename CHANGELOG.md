# Changelog

## 2.1.1

*Released 02 June 2024*

- 🔃Updated: The HTML landing page has an updated style (using [Bulma](https://bulma.io/)).

### Docker Image: `registry.gitlab.com/upcomingmcu/api:v2.1.1`

## 2.1.0

*Released 02 June 2024*

**Warning: This update causes damage to previous applications. Please update your applications accordingly.**

- ✨Added: The `next` and `previous` properties on `NextProductionDto`.
    - These properties can either be null or the URL of the next or previous production released.
- ✨Added: The `BASE_URL` environment variable.
    - Please set this value to the *base* URL of your app. For example: if the /productions endpoint
      is "https://umcu.app/api/productions" then set the value to "https://umcu.app/".
- 🔃Updated: The `next_production` property on `NextProductionDto` has been renamed to `production`.
- 🔃Updated: The `poster_url` property was changed to `poster_path` on `ProductionDto`.
    - Rather than giving the entire image url we now only give the path (as it is stored on TMDB). To get the entire
      url, please see [Image Basics - TMDB API](https://developer.themoviedb.org/docs/image-basics).
- 🔃Updated: The `release_date` property on `ProductionDto` is now set to the US theatrical (or digital) release date.
- 🔃Updated: The `media_type` property on `ProductionDto` now returns a String value rather than an object.
- ❌Removed: The `following_production` property on `NextProductionDto`.
    - Removed in favor of two new properties.
- ❌Removed: The `tagline` and `runtime` properties on `ProductionDto`.
    - These properties were seen as unnecessary to the API.

### Docker Image: `registry.gitlab.com/upcomingmcu/api:v2.1.0`

## 2.0.0

*Released 10 April 2024*

**Warning: This update causes damage to previous applications. Please update your applications accordingly.**

- ✨Added: Static `index.html` file at the root of the application.
    * Displays basic information about the API and links to the endpoint(s) and OpenAPI specs.
- ✨Added: 2 new properties for `ProductionDto`.
    * `tagline` and `runtime` - both are nullable values. Currently any TV series always has a null runtime.
- ✨Added: Dependency `org.springframework.boot:spring-boot-starter-aop`.
- 🔃Updated: The `/api` path has be prepended to all resources (other than any static HTML files).
    * For example, `/productions` is now `/api/productions`.
- 🔃Updated: OpenAPI specification.
    * The application info, contact, license, servers, etc. are now up-to-date with what they should be.
- 🔃Updated: Default rate limit to 3600 requests per hour.
    * Each response now indicates, with headers, the number of requests remaining, total requests, and time until rate
      limit reset (in nanoseconds).
    * If an HTTP 429 error is thrown, it will display the time until the rate limit reset (mm:ss).
    * `RATE_LIMIT` environment variable has become `RATE_LIMIT_TOKENS`.
    * Added `RATE_LIMIT_DURATION` environment variable.
- 🔃Updated: Property names for `NextProductionDto`
    * `production` becomes `next_production`.
    * `next_production` becomes `following_production`.
- 🛠️Fix: Improved performance and cleaned code to be more concise.
- ❌Removed: Numerous dependencies that were left unused.
    - `org.springframework.boot:spring-boot-starter-data-rest`
    - `org.springframework.boot:spring-boot-starter-actuator`
    - `io.micrometer:micrometer-registry-prometheus`

### Docker Image: `registry.gitlab.com/upcomingmcu/api:v2.0.0`

## 1.1

*Released 10 March 2024*

- Added: Rate limit of 100 requests per minute.
    - The rate limit can be modified in the application.properties file or as an environment variable.
    - Only applies to the `/production` endpoint(s).

### Docker Image: `registry.gitlab.com/upcomingmcu/api:v1.1`

## 1.0

*Released 10 March 2024*

Initial release.

### Docker Image: `registry.gitlab.com/upcomingmcu/api:v1.0`
